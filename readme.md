# Kite WebBrowser
A simple, lightweight web browser built with Python/PyQt.  Kite utilizes the WebKit web browser engine that comes with PyQt.  Kite can be used as a plugin for other apps.

<img src="screenshots/screenshot_home.png" style="max-width:400px;">
<img src="screenshots/screenshot_python.png" style="max-width:400px;">

## Features
- JavaScript and other plugins can be enabled
- No tabs - just a simple single page that can launch a new window
- Refresh, Stop, Back, Forward - but no history beyond the current session
- Find in page
- Developer console and inspector
- Simple settings
- Works on Linux, Mac, and Windows

## Requirements
- Python 2.7
- PyQt4

## Installation
1. Download the repository
2. Navigate to the kite_webbrowser directory
3. Run **python kite_webbrowser.py**

## Settings
Settings are located in settings.json
- inspector - allow right click inspect and the developer console
- statusbar - show statusbar
- networkManager - enable which will keep logins saved for the session
- proxy_path - optional proxy path
- createNewWindow - allow opening links in a new window (launches another Kite window)
- pluginsEnabled - plugins from other browsers such as Java and Flash can be utilized.  This is turned off by default
- javascriptEnabled - allow JavaScript.  This is true by default
- userAgent - specify a useragent string to use when browsing
- loadDefaultHome - load the default home page on startup

## Miscellaneous
- type *home* in the url to go to the default home page